# README #

Header
 * Author: Aaron Arvey
 * Date: 2015-10-20
 * Title: Code to reproduce all results in 'Genetic and epigenetic variation in the lineage specification of regulatory T cells', eLife, 2015

This code can do the following: 
  * Align RNA-seq, ChIP-seq, and other sequencing data to human and mouse genomes
  * Identify orthologous regions between mouse and human and quantify RNA-seq and ChIP-seq at these loci
  * Find lineage-specific epigenetic conservation between mouse and human
  * Determine genotype of primary human samples from ChIP-seq data and allele-specific expression of epigenetic markers
  * Assess overlap of ChIP-seq peaks with loci containing polymorphisms associated with disease risk


License

    Copyright (C) 2015  Aaron Arvey

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
